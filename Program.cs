﻿// See https://aka.ms/new-console-template for more information
using AdventOfCode2023aspDOTnet.Models.Entities;
using AdventOfCode2023aspDOTnet.PuzzleLogic;

var dayInput = new Day();

Console.WriteLine("Welcome to Manouk's Advent of Code 2023 console app!\n");
Console.WriteLine("---\n");
Console.WriteLine("Please enter the number of the day you want to solve!");
int day = int.Parse(Console.ReadLine());
//int day = 7;
if (day == 0 || day == null) { day = 1; } else if (day > 31) { day = 31; };

dayInput.Id = day;

Console.WriteLine("\nWhich Part of day " + day + " are you solving? (1/2)");
int part = int.Parse(Console.ReadLine());
//int part = 1;
if (part == 0 || part > 2) 
{ 
    Console.WriteLine("Invalid part"); 
} else {
    dayInput.Part = part;

    PuzzleBase.Solve(dayInput);
    Console.ReadLine();
};

