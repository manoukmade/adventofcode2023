﻿using AdventOfCode2023aspDOTnet.Models.Puzzle;
using System.Text.RegularExpressions;

namespace AdventOfCode2023aspDOTnet.Models.Entities
{
    public class Solution
    {
        public string? Answer { get; set; }
        public string? Message { get; set; }

    }


}
