﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2023aspDOTnet.Models.Entities
{
    public class Day
    {
        public int Id { get; set; }
        public int Part { get; set; }

        public Day()
        {
        }
       
    }
}
