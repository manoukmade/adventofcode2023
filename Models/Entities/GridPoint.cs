﻿namespace AdventOfCode2023aspDOTnet.Models.Entities;

public class GridPoint
{
    public int x;
    public int y;
    public char c;
}
