﻿namespace AdventOfCode2023aspDOTnet.Models.Puzzle
{
    public class FertilizerMap
    {
        public int Step { get; set; }
        public long Range { get; set; }
        public long Destination { get; set; }
        public long SourceStart { get; set; }
       
    }
}
