﻿namespace AdventOfCode2023aspDOTnet.Models.Puzzle;

public class PokerHand
{
    public string? Hand { get; set; }
    public List<int> HandInt { get; set; } = new List<int>();
    public Handtype Type { get; set; }
    public int Bid { get; set; }
    //public int Rank { get; set; }
}

public enum Handtype
{
    FiveOfAKind = 6,
    FourOfAKind = 5,
    FullHouse = 4,
    ThreeOfAKind = 3,
    TwoPair = 2,
    OnePair = 1,
    HighCard = 0,
}

