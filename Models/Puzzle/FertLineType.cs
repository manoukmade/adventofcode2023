﻿namespace AdventOfCode2023aspDOTnet.Models.Puzzle
{
    public enum FertLineType
    {
        SeedLine,
        MapHeader,
        EntryLine
    }
}
