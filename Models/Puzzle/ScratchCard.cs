﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2023aspDOTnet.Models.Puzzle
{
    public class ScratchCard
    {
        public int Id { get; set; }
        public int? Score { get; set; } = 0;
        public int Amount { get; set; } = 1;
        public List<int>? WinningNumbers { get; set; }
        public List<int>? MyNumbers { get; set; }
        public List<int>? MatchingNumbers { get; set; }
    }
}
