﻿namespace AdventOfCode2023aspDOTnet.Models.Puzzle;

public enum Instance
{
    Digit,
    Symbol
}

public class FoundInstance
{
    public int Row { get; set; }
    public int Position { get; set; }
    public Instance TypeInstance { get; set; } //1 is digit, 2 is symbol -- moet enum zijn
    public string? Symbol { get; set; }
    public bool IsPartNumber { get; set; } = false;
    public bool Selected { get; set; } = false;
    public int Link { get; set; }
}
