﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2023aspDOTnet.Models.Puzzle
{

    public class PartNumber
    {
        public int AssembledNumber { get; set; }
        public int? Link { get; set; }
    }

    
}
