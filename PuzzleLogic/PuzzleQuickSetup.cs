﻿using AdventOfCode2023aspDOTnet.Models.Entities;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class X
    {
        public static Solution SolveX(int part)
        {
            string line;
            int answer = 0;

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D10-Input-test.txt"));
                line = sr.ReadLine();
                while (line is not null)
                {
                    Console.WriteLine(line);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return new Solution { Answer = answer.ToString(), Message = "" };
        }
    }
}
