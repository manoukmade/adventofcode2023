﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class PipeMaze
    {
        public static Solution SolvePipeMaze(int part)
        {
            Dictionary<string, char> maze = new();
            Dictionary<int[], string> dict = new()
            {
                { new[] { 0,-1 }, "S|LJ" },
                { new[] { 1,0 }, "S-LF" },
                { new[] { 0, 1 }, "S|7F" },
                { new[] { -1,0 }, "S-J7" }
            }; //point object of naar tuple kijken
            //List<string> visualGrid = new();
            int answer = 0;

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D10-Input.txt"));
                int y = 0;
                int[] start = new int[0];
                string line = sr.ReadLine();
                while (line is not null)
                {
                    //visualGrid.Add(line);
                    for (int x = 0; x < line.Length; x++)
                    {
                        maze.Add((x + "." + y), line[x]);
                        if (line[x] == 'S')
                            start = new[] { x, y };
                    }
                    y++;
                    line = sr.ReadLine();
                }
                sr.Close();

                List<List<int[]>> paths = GetMazePaths(start, maze, dict);
                answer = CountLongestPath(start, paths);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return new Solution { Answer = answer.ToString(), Message = "" };
        }

        public static List<List<int[]>> GetMazePaths(int[] start, Dictionary<string, char> maze, Dictionary<int[], string> dict)
        {
            List<List<int[]>> allPaths = new();
            int[] startPoint = new int[] {-1,-1};
            int startX = start[0];
            int startY = start[1];
            int[] previousPoint = start;
            int[] previousDir = new int[] { 0, 0 };
            List<int[]> startDirections = GetDirections(start, previousDir, maze, dict);
            foreach (int[] newpoint in startDirections)
                allPaths.Add(new List<int[]>() { start, newpoint });

            for (int i = 0; i < allPaths.Count; i++)
            {
                startPoint = new int[] { allPaths[i].Last()[0], allPaths[i].Last()[1] };
                while (startPoint[0] != startX || startPoint[1] != startY)
                {
                    if (startPoint[0] == previousPoint[0] && startPoint[1] == previousPoint[1])
                    {
                        allPaths.RemoveAt(i);
                        i--;
                        break;
                    }    
                    
                    int[] originDir = new int[] { allPaths[i].Last()[2], allPaths[i].Last()[3] };
                    List<int[]> followingDirections = GetDirections(startPoint, originDir, maze, dict);
                    if (followingDirections.Count == 1)
                        allPaths[i].Add(followingDirections[0]);
                    else
                    {
                        allPaths.RemoveAt(i);
                        i--;
                        break;
                    }
                    previousPoint = startPoint;
                    startPoint = new int[] { allPaths[i].Last()[0], allPaths[i].Last()[1] };
                }
                previousPoint = new int[] { startX, startY };
            }
            return allPaths;
        }

        public static List<int[]> GetDirections(int[] current, int[] previousDir, Dictionary<string, char> maze, Dictionary<int[], string> dict)
        {
            List<int[]> availableDirections = new();
            List<int[]> outputPoints = new();
            ReverseDirection(previousDir);
            foreach (var pair in dict)
                if (pair.Value.Contains(maze[current[0] + "." + current[1]]) && (pair.Key[0] != previousDir[0] || pair.Key[1] != previousDir[1]))
                    availableDirections.Add(pair.Key);

            foreach (int[] dir in availableDirections)
            {
                //Console.WriteLine("Direction found: (" + dir[0] + "," + dir[1] + ")");
                int[] nextPoint = LookForValidPath(current, dir, maze, dict);
                //Console.WriteLine("--> " + nextPoint[0] + "." + nextPoint[1]);
                if (nextPoint[0] != current[0] || nextPoint[1] != current[1])
                    outputPoints.Add(nextPoint.Concat(dir).ToArray());
                //else
                //    Console.WriteLine("------> There's a collision");

            }
            return outputPoints;
        }

        public static int[] LookForValidPath(int[] current, int[] availableDir, Dictionary<string, char> maze, Dictionary<int[], string> dict)
        {
            int x = current[0] + availableDir[0];
            int y = current[1] + availableDir[1];
            char found = maze[x + "." + y];
            int[] validDir = new int[] { availableDir[0], availableDir[1] }; // om te kijken of adjacent character ook mag ontvangen
            ReverseDirection(validDir);
            if (found != '.')
            {
                if(dict.FirstOrDefault(x => x.Value.Contains(found) && x.Key[0] == validDir[0] && x.Key[1] == validDir[1]).Key is not null)
                {
                    return new int[] { x, y };
                }        
            }
            return current;

        }

        public static int CountLongestPath(int[] start, List<List<int[]>> allPaths)
        {
            int result = 0;
            foreach (var path in allPaths)
            {
                if (path.Last()[0] == start[0] && path.Last()[1] == start[1])
                {
                    int count = (path.Count() - 1) / 2;
                    if (count > result) result = count;
                }
            }
            return result;
        }

        public static void ReverseDirection(int[] direction)
        {
            /// invert a directional input to reverse where we came from or to see if next potential path can "receive" us
            for (int i = 0; i < direction.Count(); i++)
                if (direction[i] != 0)
                    direction[i] *= -1;
        }
    }
}
