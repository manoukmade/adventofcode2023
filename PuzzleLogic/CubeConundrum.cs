﻿using AdventOfCode2023aspDOTnet.Models.Entities;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class CubeConundrum
    {
        public static Solution SolveCubeConundrum(int part)
        {
            int answer = 0;
            Dictionary<string, int> bag = new() { { "red", 12 }, { "green", 13 }, { "blue", 14 } };
            using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D2-Input.txt"));
            string line = sr.ReadLine();
            while (line != null)
            {
                List<string> game = line.Split(new[]{ "Game", ":", ",", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                int gameId = Convert.ToInt32(game[0]);
                game.RemoveAt(0);
                _ = part == 1 && CheckBag(game, bag) ? answer += gameId : answer += ExtractMinColors(game);
                line = sr.ReadLine();
            }
            sr.Close();
            return new Solution { Answer = answer.ToString(), Message = "Conundrum is fixed!" };
        }

        public static int ExtractMinColors(List<string> game)
        {
            Dictionary<string, int> gameBag = new(); 
            int result = 1;
            foreach (string c in game)
            {
                List<string> color = c.Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();
                int colorValue = Convert.ToInt32(color[0]);
                gameBag.TryGetValue(color[1], out int gameCount);
                if (colorValue > gameCount)
                    gameBag[color[1]] = colorValue;
            }
            foreach (var cPair in gameBag)
                result *= cPair.Value;
            return result;
        }

        public static bool CheckBag(List<string> game, Dictionary<string, int> bag)
        {
            foreach (string c in game)
            {
                List<string> color = c.Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();
                bag.TryGetValue(color[1], out int count);
                if (Convert.ToInt32(color[0]) > count)
                    return false;
            }
            return true;
        }
    }
}
