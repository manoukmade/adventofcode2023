﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using AdventOfCode2023aspDOTnet.Models.Puzzle;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class ScratchCards
    {
        public static Solution SolveScratchCards(int part)
        {
            string scratchCards;
            List<ScratchCard> cards = new();
            Solution result = new();

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D4-Input.txt"));
                scratchCards = sr.ReadLine();
                while (scratchCards is not null)
                {
                    ScratchCard card = CreateCard(scratchCards);
                    cards.Add(card);
                    scratchCards = sr.ReadLine();
                }
                sr.Close();

                if (part ==2)
                    DuplicateCards(cards);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                int answer = cards.Aggregate(0, func: (sum, card) => (int)(sum + (part != 1 ? card.Amount : card.Score)));
                result.Answer = answer.ToString();
                result.Message = "You won the lottery";
            }
            return result;
        }

        public static ScratchCard CreateCard(string scTextLine)
        {

            ScratchCard result = new();
            string[] seperationStrings = { "Card", ":", "|" };
            string[] cardElements = scTextLine.Split(seperationStrings, StringSplitOptions.RemoveEmptyEntries);

            List<string> winningNumbers = cardElements[1].Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
            List<string> myNumbers = cardElements[2].Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();

            result.Id = Convert.ToInt32(cardElements[0]);

            result.WinningNumbers = PuzzleBase.TranslateStringsToInt(winningNumbers).ToList();
            result.MyNumbers = PuzzleBase.TranslateStringsToInt(myNumbers).ToList();

            result.MatchingNumbers = result.WinningNumbers.Intersect(result.MyNumbers).ToList(); 
            if (result.MatchingNumbers.Count > 0)
                CalculateScore(result); 

            return result;
        }

        public static void CalculateScore(ScratchCard card)
        {
            card.Score = 1;
            int multiply = card.MatchingNumbers.Count - 1;
            for (var i = 0; i < multiply; i++)
            {
                card.Score *= 2;
            };
        }

        public static void DuplicateCards(List<ScratchCard> cards)
        {
            foreach (var card in cards)
            {
                if (card.MatchingNumbers.Count > 0)
                {
                    for (var i = 0; i < card.MatchingNumbers.Count; i++)
                    {
                        int updateCardId = card.Id + i;
                        cards[updateCardId].Amount += card.Amount;
                    }
                }
            }
        }
    }
}
