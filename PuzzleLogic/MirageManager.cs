﻿using AdventOfCode2023aspDOTnet.Models.Entities;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class MirageManager
    {
        public static Solution SolvingOasisReport(int part)
        {
            int answer = 0;

                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D9-Input.txt"));
                string line = sr.ReadLine();
                while (line is not null)
                {
                    List<List<int>> diagram = CreateDiagram(PuzzleBase.TranslateStringsToInt(line.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList()));
                    _ = part == 1 ? answer += ExtrapolateForward(diagram) : answer += ExtrapolateBackward(diagram);
                    line = sr.ReadLine();
                }
                sr.Close();
            
            return new Solution { Answer = answer.ToString(), Message = "Oasis Found" };
        }

        public static List<List<int>> CreateDiagram(List<int> lineInts)
        {
            List<List<int>> diagram = new() { lineInts };
            Dictionary<int, int> valueCount = new();
            while (valueCount.Count != 1)
            {
                valueCount.Clear();
                List<int> row = new();
                for (int i = 1; i < diagram.First().Count; i++)
                {
                    int newValue = diagram.First()[i] - diagram.First()[i - 1];
                    row.Add(newValue);
                    valueCount.TryGetValue(newValue, out int count);
                    valueCount[newValue] = count + 1;
                }
                diagram.Insert(0, row);
            }
            return diagram;
        }

        public static int ExtrapolateForward(List<List<int>> diagram)
        {
            for (int r = 1; r < diagram.Count; r++)
                diagram[r].Add(diagram[r].Last() + diagram[r - 1].Last());
            return diagram.Last().Last();
        }

        public static int ExtrapolateBackward(List<List<int>> diagram)
        {
            for (int r = 1; r < diagram.Count; r++)
                diagram[r].Insert(0,diagram[r].First() - diagram[r-1].First());
            return diagram.Last().First();
        }
    }
}
