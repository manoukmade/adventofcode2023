﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using System.Text;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class LensLibrary
    {
        public static Solution SolveInitializationManual(int part)
        {
            int answer = 0;
            int divider = 256;
            int counter = 0; // https://learn.microsoft.com/en-us/dotnet/api/system.collections.generic.queue-1?view=net-8.0
            char[] operation = new [] { '=', '-' };
            Dictionary<string, Tuple<int, int>> lensLibrary = new();

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D15-Input.txt"));
                string line = sr.ReadLine();
                while (line is not null)
                {
                    List<string> input = line.Split(',').ToList();
                    foreach (string s in input)
                        if (part == 1)
                            answer += HashAlgoritmn(s, divider);
                        else
                        {
                            string[] lensInput = s.Split(operation, StringSplitOptions.RemoveEmptyEntries);
                            string labelHash = $"{HashAlgoritmn(lensInput[0], divider)}.{lensInput[0]}";
                            if (lensInput.Count() == 1)
                                lensLibrary.Remove(labelHash);
                            else
                            {
                                if (!lensLibrary.ContainsKey(labelHash)) { 
                                    lensLibrary.Add(labelHash, new Tuple<int, int>(counter, Convert.ToInt32(lensInput[1])));
                                    counter++;
                                }
                                else {
                                    int existingPlace = lensLibrary[labelHash].Item1;
                                    lensLibrary.Remove(labelHash);
                                    lensLibrary.Add(labelHash, new Tuple<int, int>(existingPlace, Convert.ToInt32(lensInput[1])));
                                }
                            }
                        }
                    line = sr.ReadLine();
                }
                sr.Close();
                if (part == 2)
                    answer = CalculateLensSum(lensLibrary, divider);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return new Solution { Answer = answer.ToString(), Message = "Lava Production Facility is online!" };
        }

        public static int HashAlgoritmn(string sequence, int divider)
        {
            int output = 0;
            int multiplier = 17;
            byte[] asciiBytes = Encoding.ASCII.GetBytes(sequence);
            foreach (byte input in asciiBytes)
                output = ((output + input) * multiplier) % divider;
            return output;
        }

        public static int CalculateLensSum(Dictionary<string, Tuple<int,int>> dictionary, int boxes)
        {
            int result = 0;
            for (int b = 0; b < boxes; b++)
            {
                var boxList = dictionary.Where(x => x.Key.StartsWith(b + ".")).OrderBy(l => l.Value.Item1).ToList();
                for (int i = 0; i < boxList.Count(); i++)
                    result += ((b + 1) * (i + 1) * boxList[i].Value.Item2);
            }
            return result;
        }
    }
}
