﻿using AdventOfCode2023aspDOTnet.Models.Entities;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class CosmicExpansion
    {
        public static Solution SolveObservationAnalysis(int part)
        {
            int row = 0;
            long modifier;
            if (part == 1)
                modifier = 2;
            else
                modifier = 1000000;

            List<GridPoint> galaxy = new();
            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D11-Input.txt"));
                string line = sr.ReadLine();
                while (line is not null)
                {
                    int spaceCount = 0;
                    for (int c = 0; c < line.Length; c++)
                    {
                        galaxy.Add(new GridPoint() { x = c, y = row, c = line[c] });
                        if (line[c] == '.')
                            spaceCount++;
                    }
                    if (spaceCount == line.Length)
                    {
                        for (int c = 0; c < line.Length; c++)
                            galaxy[galaxy.Count - c - 1].c = '*';
                    }
                    row++;
                    line = sr.ReadLine();
                }
                sr.Close();
                MarkVerticalSpace(galaxy);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return new Solution { Answer = FindSumShortedPaths(galaxy, modifier).ToString(), Message = "" };
        }

        public static void MarkVerticalSpace(List<GridPoint> galaxy)
        {
            for (int i = galaxy.Last().x; i >= 0; i--)
            {
                List<GridPoint> verticalPoint = galaxy.Where(p => p.x == i).ToList();
                int spaceCount = 0;
                for (int c = 0; c < verticalPoint.Count; c++)
                    if (verticalPoint[c].c == '.' || verticalPoint[c].c == '*')
                        spaceCount++;
                if (spaceCount == verticalPoint.Count)
                {
                    foreach (GridPoint p in verticalPoint)
                    {
                        galaxy.Remove(p);
                        galaxy.Add(new GridPoint() { x = p.x, y = p.y, c = '*' });
                    }
                }
            }
        }

        public static long FindSumShortedPaths(List<GridPoint> galaxy, long modifier)
        {
            Dictionary<string, char> galaxyDict = new();
            foreach (GridPoint p in galaxy)
                galaxyDict.Add((p.x + "." + p.y).ToString(), p.c);
                
            List<GridPoint> allGalaxies = galaxy.Where(o => o.c == '#').ToList(); 

            long result = 0;

            for (int g = 0; g < allGalaxies.Count - 1; g++)
                for (int i = 1; i < allGalaxies.Count - g; i++)
                {
                    List<GridPoint> working = new() { allGalaxies[g], allGalaxies[g + i] };
                    long path = CalculatePathLength(working, galaxyDict, modifier);
                    result += path;
                }
            return result;
        }

        public static long CalculatePathLength(List<GridPoint> pair, Dictionary<string, char> galaxy, long modifier)
        {
            long result = 0;
            long count = 0;
            string[] axis = new[] { "x", "y" };

            foreach (string a in axis)
            {
                List<GridPoint> compare = new();
                compare = pair.OrderBy(o => (a == "x" ? o.x : o.y)).ToList();
                int limit = a == "x" ? compare[1].x - compare[0].x : compare[1].y - compare[0].y;
                result += limit;
                for (int p = 1; p < limit; p++)
                {
                    string coordinate = a == "x" ? (compare[0].x + p).ToString() + ".0" : "0." + (compare[0].y + p).ToString(); 
                    if (galaxy[coordinate] == '*')
                        count++;
                }
            }

            return result - count + (count * modifier);
        }
    }
}
