﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using AdventOfCode2023aspDOTnet.Models.Puzzle;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class CamelCards
    {
        public static Solution SolveCamelCardWinnings(int part)
        {
            Solution result = new();
            string line;
            int answer = 0;
            List<PokerHand> pokerHands = new();
            
            Dictionary<char, int> pokerValues = new()
            {   { '2', 2 },
                { '3', 3 },
                { '4', 4 },
                { '5', 5 },
                { '6', 6 },
                { '7', 7 },
                { '8', 8 },
                { '9', 9 },
                { 'T', 10 },
                { 'Q', 12 },
                { 'K', 13 },
                { 'A', 14 }
            };
            if (part == 1)
                pokerValues.Add('J', 11);
            else
                pokerValues.Add('J', 1);

                try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D7-Input.txt"));
                line = sr.ReadLine();
                while (line is not null)
                {
                    string[] splitLine = line.Split(' ');
                    PokerHand newHand = new() { Hand = splitLine[0], Bid = Convert.ToInt32(splitLine[1]) };
                    Dictionary<char, int> cardCount = new();

                    foreach (char card in splitLine[0])
                    {
                        newHand.HandInt.Add(pokerValues[card]);
                        cardCount.TryGetValue(card, out int count);
                        cardCount[card] = count + 1;
                    }

                    if (part == 2)
                        AssigningJokers(cardCount);

                    if (cardCount.Count == 1)
                        newHand.Type = Handtype.FiveOfAKind;
                    else if (cardCount.Count == 5)
                        newHand.Type = Handtype.HighCard;
                    else
                    {
                        var sortedCards = from entry in cardCount orderby entry.Value descending select entry;
                        newHand.Type = DetermineHandType(sortedCards);
                    }
                    pokerHands.Add(newHand);
                    line = sr.ReadLine();
                }
                sr.Close();

                var orderedHands = pokerHands.OrderBy(x => x.Type)
                    .ThenBy(x => x.HandInt[0])
                    .ThenBy(x => x.HandInt[1])
                    .ThenBy(x => x.HandInt[2])
                    .ThenBy(x => x.HandInt[3])
                    .ThenBy(x => x.HandInt[4]).ToList();

                for (int c = 0; c < orderedHands.Count; c++)
                {
                    int winnings = orderedHands[c].Bid * (1 + c);
                    answer += winnings;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                result.Answer = answer.ToString();
                result.Message = "Collect your prize money at your Camelbank";
            }
            return result;
        }

        public static Handtype DetermineHandType(IOrderedEnumerable<KeyValuePair<char, int>> sortedCards)
        {
            int previousPair = 0;
            foreach (var pair in sortedCards)
            {
                if (pair.Value == 4)
                    return Handtype.FourOfAKind;
                else if (pair.Value == 3)
                    previousPair = pair.Value;
                else if (pair.Value == 2)
                {
                    if (previousPair == 3)
                        return Handtype.FullHouse;
                    if (previousPair == 2)
                        return Handtype.TwoPair;
                    previousPair = pair.Value;
                }
                else {
                    if (previousPair == 3)
                        return Handtype.ThreeOfAKind;
                    else
                        return Handtype.OnePair;
                }
            }
            return Handtype.HighCard;
            
        }

        public static void AssigningJokers(Dictionary<char, int> countedHand)
        {
            countedHand.TryGetValue('J', out int jokerCount);
            if (jokerCount > 0)
            {
                var sorting = from entry in countedHand orderby entry.Value descending select entry;
                foreach (var pair in sorting)
                {
                    if (pair.Key != 'J')
                    {
                        countedHand[pair.Key] = pair.Value + jokerCount;
                        countedHand.Remove('J');
                        break;
                    }
                }
            }
        }
    }
}

