input = `two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen`

/* verbetering
maps = {
    'one':1,
    'two':2,
    'three':3,
    'four':4,
    'five':5,
    'six':6,
    'seven':7,
    'eight':8,
    'nine': 9
}
*/

const inputArray = input.split(`\n`);

let outputSum = 0;

for (let i = 0; i < inputArray.length; i++) {
    let firstDigit = inputArray[i].match(/(?:([0-9])|(\W*(one|two|three|four|five|six|seven|eight|nine)\W*))/)
    const lastDigitregex = /.*(one|two|three|four|five|six|seven|eight|nine|[0-9]).*?$/gm;
    let lastDigit = lastDigitregex.exec(inputArray[i]);
    let combinedResult = Number(translateNumber(firstDigit[0]) + translateNumber(lastDigit[1]))
    outputSum = outputSum + combinedResult;
};

console.log(outputSum);

function translateNumber (digit) {
    let translateResult;
switch(digit){
    case "one":
        translateResult = "1";
        break;
    case "two":
        translateResult = "2";
        break;
    case "three":
        translateResult = "3";
        break;
    case "four":
        translateResult = "4";
        break;
    case "five":
        translateResult = "5";
        break;
    case "six":
        translateResult = "6";
        break;
    case "seven":
        translateResult = "7";
        break;
    case "eight":
        translateResult = "8";
        break;
    case "nine":
        translateResult = "9"
        break;
    default:
        translateResult = digit;
    }

    return translateResult;
}