﻿using AdventOfCode2023aspDOTnet.Models.Entities;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    internal class PuzzleBase
    {
        public static string GetLocalInputFile(string filename)
        {
            string dir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string file = dir + @"\PuzzleLogic\PuzzleInput\" + filename;
            return file;
        }
        public static void Solve(Day day)
        {
            var PuzzleSolution = new Solution();
            Console.WriteLine("Solving advent for day " + day.Id + " part " + day.Part);


            switch (day.Id)
            {
                case 1:
                    Console.WriteLine("Reading Calibration Document....\n");
                    PuzzleSolution = Trebuchet.SolveTrebuchetProblem(day.Part);
                    break;
                case 2:
                    Console.WriteLine("Solving the Cube Conundrum....\n");
                    PuzzleSolution = CubeConundrum.SolveCubeConundrum(day.Part);
                    break;

                case 3:
                    Console.WriteLine("Checking Gear Ratios....\n");
                    PuzzleSolution = GearRatios.SolveGearRatios(day.Part);
                    break;
                case 4:
                    Console.WriteLine("Comparing Winning Numbers to Scratchcards....\n");
                    PuzzleSolution = ScratchCards.SolveScratchCards(day.Part);
                    break;

                case 5:
                    Console.WriteLine("Calculating Best Seeding Location....\n");
                    PuzzleSolution = Fertilizer.SolveSeedLocation(day.Part);
                    break;
                case 6:
                    Console.WriteLine("Racing to the finish line....\n");
                    PuzzleSolution = BoatRace.SolveBoatRace(day.Part);
                    break;
                case 7:
                    Console.WriteLine("Playing Camel Cards....\n");
                    PuzzleSolution = CamelCards.SolveCamelCardWinnings(day.Part);
                    break;
                case 8:
                    Console.WriteLine("Navigating the wasteland....\n");
                    PuzzleSolution = WastelandNavigator.SolveNavigationNetwork(day.Part);
                    break;
                case 9:
                    Console.WriteLine("Extrapolating values....\n");
                    PuzzleSolution = MirageManager.SolvingOasisReport(day.Part);
                    break;
                case 10:
                    Console.WriteLine("Scanning the area....\n");
                    PuzzleSolution = PipeMaze.SolvePipeMaze(day.Part);
                    break;
                case 11:
                    Console.WriteLine("Navigating the universe....\n");
                    PuzzleSolution = CosmicExpansion.SolveObservationAnalysis(day.Part);
                    break;
                case 13:
                    Console.WriteLine("Dodging the Lava....\n");
                    PuzzleSolution = Mirrors.SolveMirrorValley(day.Part);
                    break;
                default:
                    Console.WriteLine("The Puzzle has not been made for this day yet!\n");
                    break;
            };

            if (PuzzleSolution.Answer == null)
            {
                Console.WriteLine("Something went wrong while solving the puzzle.\n");
            }
            else
            {
                Console.WriteLine("Your puzzle solution for day " + day.Id + " part " + day.Part + " is " + PuzzleSolution.Answer);
                Console.WriteLine(PuzzleSolution.Message);
            };
        }
        public static List<int> TranslateStringsToInt(List<string> list)
        {
            List<int> result = new();

            foreach (var item in list)
            {
                int winningnumber = int.TryParse(item, out var assembledNumber)
                ? assembledNumber
                : 0;
                result.Add(winningnumber);
            }
            return result;
        }
        public static List<long> TranslateStringsToLong(List<string> list)
        {
            List<long> result = new();

            foreach (var item in list)
            {
                long winningnumber = long.TryParse(item, out var assembledNumber)
                ? assembledNumber
                : 0;
                result.Add(winningnumber);
            }
            return result;
        }

        public static List<long> CompileTranslateStringsToLong(List<string> list)
        {
            List<long> result = new();
            string compiled = "";
            foreach (string lString in list)
            {
                _ = compiled += lString;
            }
            long longresult = long.TryParse(compiled, out var assembledNumber)
                ? assembledNumber
                : 0;
            result.Add(longresult);
            return result;
        }
    }
}
