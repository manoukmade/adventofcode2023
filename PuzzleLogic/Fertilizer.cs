﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using AdventOfCode2023aspDOTnet.Models.Puzzle;
using System.Text.RegularExpressions;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class Fertilizer
    {
        public static Solution SolveSeedLocation(int part)
        {
            int mapSection = 0;
            List<long[]> seedList = new();
            List<FertilizerMap> mapList = new();
            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D5-Input.txt"));
                string line = sr.ReadLine();
                while (line is not null)
                {
                    if (line != string.Empty)
                    {
                        switch (DetermineLineType(line))
                        {
                            case FertLineType.SeedLine:
                                List<long> seeds = PuzzleBase.TranslateStringsToLong(line.Remove(0, 7).Split(" ").ToList());
                                seedList = (part == 1 ? CreateSingleSeeds(seeds) : CreateRangedSeeds(seeds));
                                break;
                            case FertLineType.MapHeader:
                                mapSection++;
                                break;
                            default:
                                mapList.Add(MapValues(line, mapSection));
                                break;
                        }
                    }
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return new Solution() { Answer = MapSeedsToLocation(seedList, mapList).ToString(), Message = "Best seeding location found!" };
        }

        public static FertLineType DetermineLineType(string line)
        {
            if (line.StartsWith("seeds:"))
                return FertLineType.SeedLine;
            else if (Regex.IsMatch(line, @"^\D"))
                return FertLineType.MapHeader;
            return FertLineType.EntryLine;
        }

        public static List<long[]> CreateSingleSeeds(List<long> seeds)
        {
            List<long[]> result = new();
            for (int i = 0; i < seeds.Count; i++)
                result.Add(new long[] { seeds[i], 1 });
            return result;
        }

        public static List<long[]> CreateRangedSeeds(List<long> seeds)
        {
            List<long[]> result = new();
            for (int i = 0; i < seeds.Count; i += 2)
                result.Add(new long[] { seeds[i], seeds[i] + seeds[i + 1] - 1 });
            return result;
        }

        public static FertilizerMap MapValues(string line, int mapSection)
        {
            List<string> mapStrings = line.Split(' ').ToList();
            List<long> mapValues = new();

            foreach (string item in mapStrings)
            {
                long converted = Convert.ToInt64(item);
                mapValues.Add(converted);
            }

            return new FertilizerMap
            {
                Step = mapSection,
                Range = mapValues[2],
                Destination = mapValues[0],
                SourceStart = mapValues[1]
            };
        }

        public static long MapSeedsToLocation(List<long[]> seedList, List<FertilizerMap> mapList)
        {
            Dictionary<int, List<FertilizerMap>> validMapsByM = mapList.GroupBy(m => m.Step).ToDictionary(g => g.Key, g => g.ToList());
            List<long[]> calc = new();
            List<long[]> output = new();
            calc.AddRange(seedList);
            for (int m = 1; m < 8; m++)
            {
                if (validMapsByM.TryGetValue(m, out var validMaps))
                    foreach (FertilizerMap map in validMaps)
                    {
                        long maxMapValue = map.SourceStart + map.Range - 1;
                        long mapDifference = map.Destination - map.SourceStart;
                        if (calc.Count > 0)
                        {
                            for (int r = 0; r < calc.Count; r++)
                            {
                                long calcStart = calc[r][0];
                                long calcEnd = calc[r][1];
                                if ((calcStart >= map.SourceStart && calcStart <= maxMapValue) || (map.SourceStart >= calcStart && map.SourceStart <= calcEnd))
                                {
                                    long newRangeStart = calcStart < map.SourceStart ? map.SourceStart : calcStart;
                                    long newRangeEnd = calcEnd > maxMapValue ? maxMapValue : calcEnd;
                                    if (newRangeStart > calcStart && newRangeEnd == calcEnd)
                                        calc[r][1] = newRangeStart - 1;
                                    else if (calcStart == newRangeStart && calcEnd > newRangeEnd)
                                        calc[r][0] = newRangeEnd + 1;
                                    else if (newRangeStart == calcStart && newRangeEnd == calcEnd)
                                        calc.RemoveAt(r);
                                    else
                                    {
                                        calc[r][1] = newRangeStart;
                                        calc.Add(new[] { newRangeEnd, calcEnd });
                                    }
                                    r--;
                                    output.Add(new[] { newRangeStart + mapDifference, newRangeEnd + mapDifference });
                                }
                            }
                        }
                    }

                if (m < 7)
                {
                    calc.AddRange(output);
                    output.Clear();
                }
                else
                    output.AddRange(calc);
            }
            List<long[]> result = output.OrderBy(x => x[0]).ToList();
            return result.First()[0];
        }
    }
}





