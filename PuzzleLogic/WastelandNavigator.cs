﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using System.Text.RegularExpressions;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    internal class WastelandNavigator
    {
        public static Solution SolveNavigationNetwork(int part)
        {
            Solution result = new() { Message = "You navigated the Wasteland!" };
            Dictionary<string, List<string>> diagram = new();
            List<string> startingStrings = new();

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D8-Input.txt"));
                string map = sr.ReadLine();
                string line = sr.ReadLine();
                while (line is not null)
                {
                    MatchCollection matchList = Regex.Matches(line, @"\w+", options: RegexOptions.None);
                    List<string> lineList = matchList.Cast<Match>().Select(match => match.Value).ToList();
                    if (lineList.Count > 0)
                    {
                        diagram.Add(lineList[0], new List<string> { lineList[1], lineList[2] });
                        if (Regex.IsMatch(lineList[0], @"..A", options: RegexOptions.None))
                            startingStrings.Add(lineList[0]);
                    }
                    line = sr.ReadLine();
                }
                sr.Close();
                _ = part == 1 ? result.Answer = StepsToFinishHumanDiagram(map, diagram).ToString() : result.Answer = StepToFinishGhostDiagram(map, diagram, startingStrings).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return result;
        }

        public static int StepsToFinishHumanDiagram(string map, Dictionary<string, List<string>> diagram)
        {
            int steps = 0;
            int mapStep = 0;
            string currentValue = "AAA";

            while (currentValue != "ZZZ")
            {
                if (mapStep == map.Length)
                    mapStep -= map.Length;
                if (map[mapStep] == 'L')
                    currentValue = diagram[currentValue][0];
                else
                    currentValue = diagram[currentValue][1];
                mapStep++;
                steps++;
            }
            return steps;
        }

        public static long StepToFinishGhostDiagram(string map, Dictionary<string, List<string>> diagram, List<string> currentValues)
        {
            Regex EndValues = new(@"..Z+");
            List<long> valueStepList = new();

            foreach (string value in currentValues)
            {
                string currentValue = value;
                int mapStep = 0;
                long valueSteps = 0;
                while (!EndValues.IsMatch(currentValue))
                {
                    if (mapStep == map.Length)
                        mapStep -= map.Length;
                    if (map[mapStep] == 'L')
                        currentValue = diagram[currentValue][0];
                    else
                        currentValue = diagram[currentValue][1];
                    mapStep++;
                    valueSteps++;
                }
                valueStepList.Add(valueSteps);
            };

            return FindStepsThoughMath(valueStepList);
        }

        public static long FindStepsThoughMath(List<long> listValues)
        {
            List<int> primeFactors = new();
            long steps = 1;
            for (int i = 0; i < listValues.Count; i++)
            {
                long n = listValues[i];
                for (int p = 2; p <= n; p++)
                {
                    while (n % p == 0)
                    {
                        if (!primeFactors.Contains(p))
                            primeFactors.Add(p);
                            n /= p;
                    }
                }
            }

            foreach (int p in primeFactors)
                steps *= p;

            return steps;
        }
    }
}
