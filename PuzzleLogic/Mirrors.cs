﻿using AdventOfCode2023aspDOTnet.Models.Entities;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class Mirrors
    {
        public static Solution SolveMirrorValley(int part)
        {
            int answer = 0;
            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D13-Input.txt"));
                List<GridPoint> valley = new();
                int gridRow = 0;
                string line = sr.ReadLine();
                while (line is not null)
                {
                    if (line != "")
                    {
                        for (int c = 0; c < line.Length; c++)
                            valley.Add(new GridPoint() { x = c, y = gridRow, c = line[c] });
                        gridRow++;
                    }
                    else
                    {
                        _ = part == 1 ? answer += CalculateValuePerValley(valley) : answer += CalculateValuePerSmudgedValley(valley);
                        valley.Clear();
                        gridRow = 0;
                    }
                    line = sr.ReadLine();
                }
                sr.Close();
                _ = part == 1 ? answer += CalculateValuePerValley(valley) : answer += CalculateValuePerSmudgedValley(valley);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            return new Solution { Answer = answer.ToString(), Message = "" };
        }

        public static List<List<GridPoint>> CreateLists(List<GridPoint> valley, string xy)
        {
            List<List<GridPoint>> output = new();
            int limit = (xy == "y" ? valley.Last().y : valley.Last().x);
            for (int i = 0; i <= limit; i++)
            {
                List<GridPoint> line = valley.Where(r => (xy == "y" ? r.y : r.x) == i).ToList();
                _ = line.OrderBy(o => (xy == "y" ? o.y : o.x));
                output.Add(line);
            }
            return output;
        }

        public static int CalculateValuePerValley(List<GridPoint> valley)
        {
            string[] orientation = new[] { "y", "x" };
            List<List<GridPoint>> horizontalCheck = CreateLists(valley, orientation[0]);
            List<List<GridPoint>> verticalCheck = CreateLists(valley, orientation[1]);

            foreach (string value in orientation)
            {
                int row = 0;
                List<List<GridPoint>> workingList = (value == "y" ? horizontalCheck : verticalCheck);
                do
                {
                    bool compare = CompareMirror(workingList[row], workingList[row + 1]);
                    if (compare)
                    {
                        int mirrorEdge = (value == "y" ? (row + 1) * 100 : row + 1);
                        int right = row + 2;
                        int left = row - 1;
                        while (left >= 0 && right < workingList.Count && compare == true)
                        {
                            if (!CompareMirror(workingList[left], workingList[right]))
                                compare = false;
                            left--;
                            right++;
                        }
                        if (compare)
                            return mirrorEdge;
                    }
                    row++;
                } while (row < workingList.Count - 1);
            }
            return 0;
        }

        public static bool CompareMirror(List<GridPoint> left, List<GridPoint> right)
        {
            for (int i = 0; i < left.Count; i++)
                if (left[i].c != right[i].c)
                    return false;
            return true;
        }

        public static int CalculateValuePerSmudgedValley(List<GridPoint> valley)
        {
            string[] orientation = new[] { "y", "x" };
            List<List<GridPoint>> horizontalCheck = CreateLists(valley, orientation[0]);
            List<List<GridPoint>> verticalCheck = CreateLists(valley, orientation[1]);

            foreach (string value in orientation)
            {
                int row = 0;
                List<List<GridPoint>> workingList = (value == "y" ? horizontalCheck : verticalCheck);
                do
                {
                    int? smudgecount = 0;
                    smudgecount = CompareSmudgedMirror(workingList[row], workingList[row + 1], smudgecount);
                    if (smudgecount is not null)
                    {
                        int mirrorEdge = (value == "y" ? (row + 1) * 100 : row + 1);
                        int right = row + 2;
                        int left = row - 1;
                        while (left >= 0 && right < workingList.Count && smudgecount is not null)
                        {
                            smudgecount = CompareSmudgedMirror(workingList[left], workingList[right], smudgecount);
                            left--;
                            right++;
                        }
                        if (smudgecount == 1)
                            return mirrorEdge;
                    }
                    row++;
                } while (row < workingList.Count - 1);
            }
            return 0;
        }

        public static int? CompareSmudgedMirror(List<GridPoint> left, List<GridPoint> right, int? smudgecount)
        {
            for (int i = 0; i < left.Count; i++)
                if (left[i].c != right[i].c)
                    smudgecount++;
            if (smudgecount > 1)
                smudgecount = null;
            return smudgecount;
        }
    }
}
