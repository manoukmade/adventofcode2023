﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using AdventOfCode2023aspDOTnet.Models.Puzzle;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    internal class GearRatios
    {
        public static Solution SolveGearRatios(int part)
        {
            string fileLine;
            List<FoundInstance> foundList = new();
            int answer = 0;
            Solution result = new(); //nicer dat type link staat
            var digitRegex = @"\d";
            var symbolRegex = @"[^.^\d^\n]";

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D3-Input.txt"));
                fileLine = sr.ReadLine(); // do while zodat readLine op 1 plek zit?

                int row = 0;
                while (fileLine is not null)
                {
                    GetInstances(foundList, digitRegex, symbolRegex, fileLine, row); // either lijst maken of lijst als reference AddRange
                    fileLine = sr.ReadLine();
                    row++;
                }
                sr.Close(); //using blok sluiten maybe hier in plaats van dat. ??

                List<PartNumber> validPartNumbers = OutputPartNumbers(part, foundList); //maybe twee functions maken voor part 1 of part 2, dan is het iets leesbaarder
                answer = CalculateEngineSum(validPartNumbers);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                result.Answer = answer.ToString();
                result.Message = "Engine is fixed";
            }
            return result;
        }

        public static List<FoundInstance> GetInstances(List<FoundInstance> list, string digitRegex, string symbolRegex, string engine, int row)
        {
            //hier nieuwe lijst maken om terug te geven
            for (int i = 0; i < engine.Length; i++)
            {
                string c = engine[i].ToString(); // char heeft een is numeric functie dus heb ik helemaal regex niet nodig
                if (Regex.IsMatch(c, digitRegex))
                {
                    FoundInstance newItem = new() { Row = row, Position = i, TypeInstance = Instance.Digit, Symbol = c };
                    list.Add(newItem);

                }
                else if (Regex.IsMatch(c, symbolRegex)) // kan ook voor deze. is match niet doen want is een boolean
                {
                    FoundInstance newItem = new() { Row = row, Position = i, TypeInstance = Instance.Symbol, Symbol = c };
                    list.Add(newItem);
                }
                // als c een char is 
                // 
                //if (c != '.')
                //{
                //    FoundInstance newItem = new() { Row = row, Position = i, TypeInstance = Regex.IsMatch(c, digitRegex) ? 1 : 2, Symbol = c };
                //    list.Add(newItem);
                //}
            }
            return list;
        }

        public static List<PartNumber> OutputPartNumbers(int part, List<FoundInstance> list)
        {
            var foundNumbersList = new List<FoundInstance>();
            var result = new List<PartNumber>();
            int counter = 1;

            foreach (FoundInstance item in list)
            {
                if (part == 1 && item.TypeInstance == Instance.Digit)
                {
                    FindMatch(list, item);
                    foundNumbersList.Add(item);
                }
                else if (part == 2 && item.TypeInstance == Instance.Symbol && item.Symbol == "*")
                {
                    List<FoundInstance> found = FindAllMatches(list, item, counter);
                    if (found is not null) foundNumbersList.AddRange(found);
                    counter++;
                }
            }

            if (part == 2) //opsplitsen
            {
                foreach (FoundInstance item in list)
                {
                    if (item.TypeInstance == Instance.Digit && !foundNumbersList.Where(i => i.Row == item.Row).Where(i => i.Position == item.Position).Any())
                    {
                        foundNumbersList.Add(item);
                    }
                }
            }

            foreach (FoundInstance item in foundNumbersList)
            {
                if (item.IsPartNumber == true && item.Selected == false)
                {
                    var rowItems = MakeRowList(foundNumbersList, item);
                    var SinglePartNumber = AssembleNumber(rowItems, item);
                    result.Add(SinglePartNumber);
                }
            }

            if (part == 2)
            {
                var combinedPartNumbers = CombinePartNumbers(result, counter);
                result.Clear();
                result.AddRange(combinedPartNumbers);
            }

            return result;
        }

        public static void FindMatch(List<FoundInstance> list, FoundInstance item)
        {
            //row / position een range van maken. 
            item.IsPartNumber = list.Where(i => i.TypeInstance == Instance.Symbol)
                                    .Where(i => i.Row == item.Row || i.Row == item.Row - 1 || i.Row == item.Row + 1)
                                    .Where(i => i.Position == item.Position - 1 || i.Position == item.Position || i.Position == item.Position + 1)
                                    .Any();

            //return item;
        }

        public static List<FoundInstance> FindAllMatches(List<FoundInstance> list, FoundInstance item, int counter)
        {
            var foundList = list.Where(i => i.TypeInstance == Instance.Digit)
                                .Where(i => i.Row == item.Row || i.Row == item.Row - 1 || i.Row == item.Row + 1)
                                .Where(i => i.Position == item.Position - 1 || i.Position == item.Position || i.Position == item.Position + 1)
                                .ToList(); //hoeft niet
            foreach (var founditem in foundList)
            {
                founditem.IsPartNumber = true;
                founditem.Link = counter;

            }

            return foundList;
        }


        public static List<FoundInstance> MakeRowList(List<FoundInstance> list, FoundInstance item)
        {

            return list.Where(x => x.Row == item.Row).ToList();
        }

        public static PartNumber AssembleNumber(List<FoundInstance> list, FoundInstance item)
        {
            List<FoundInstance> foundItems = new();
            PartNumber partnumber = new();
            item.Selected = true;
            foundItems.Add(item);

            //functie maken die hem offset. item position + offset

            if (list.Any(i => i.Row == item.Row && i.Position == item.Position - 1))
            {
                FoundInstance foundBeforeOne = list.First(i => i.Row == item.Row && i.Position == item.Position - 1);
                foundBeforeOne.Selected = true;
                foundItems.Add(foundBeforeOne);
                if (list.Any(i => i.Row == foundBeforeOne.Row && i.Position == foundBeforeOne.Position - 1))
                {
                    FoundInstance foundBeforeTwo = list.First(i => i.Row == foundBeforeOne.Row && i.Position == foundBeforeOne.Position - 1);
                    foundBeforeTwo.Selected = true;
                    foundItems.Add(foundBeforeTwo);
                }

            }

            if (list.Any(i => i.Row == item.Row && i.Position == item.Position + 1))
            {
                FoundInstance foundAfterOne = list.First(i => i.Row == item.Row && i.Position == item.Position + 1);
                foundAfterOne.Selected = true;
                foundItems.Add(foundAfterOne);
                if (list.Any(i => i.Row == foundAfterOne.Row && i.Position == foundAfterOne.Position + 1))
                {
                    FoundInstance foundAfterTwo = list.First(i => i.Row == foundAfterOne.Row && i.Position == foundAfterOne.Position + 1);
                    foundAfterTwo.Selected = true;
                    foundItems.Add(foundAfterTwo);
                }

            }

            var partNumberItems = foundItems.OrderBy(obj => obj.Position).ToList();
            var assembleStringBuilder = new StringBuilder();
            foreach (var part in partNumberItems)
            {
                assembleStringBuilder.Append(part.Symbol);
                if (part.Link != 0 || partnumber.Link <= part.Link)
                    partnumber.Link = part.Link;
            }
            partnumber.AssembledNumber = int.TryParse(Regex.Match(input: assembleStringBuilder.ToString(), @"\d+").Value, out var assembledNumber)
                ? assembledNumber
                : 0;
            return partnumber;
        }

        public static int CalculateEngineSum(List<PartNumber> partnumbers)
        {
            return partnumbers.Aggregate(0, (sum, partnumber) => sum + partnumber.AssembledNumber);
        }

        public static List<PartNumber> CombinePartNumbers(List<PartNumber> partnumbers, int counter)
        {
            List<PartNumber> result = new();
            for (int c = 0; c < counter; c++)
            {
                int sum = 0;
                var foundList = partnumbers.Where(i => i.Link == c).ToList();
                if (foundList.Count == 2)
                {
                    sum = foundList[0].AssembledNumber * foundList[1].AssembledNumber;
                    PartNumber newPart = new() { AssembledNumber = sum, Link = c };
                    result.Add(newPart);
                }
            }

            return result;
        }
    }
}
