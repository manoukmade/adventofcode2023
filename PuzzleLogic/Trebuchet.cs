﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class Trebuchet
    {
        
        public static Solution SolveTrebuchetProblem(int part)
        {
            string line;
            int answer = 0;
            Solution result = new Solution();
            var firstDigitRegex = @"(?:([0-9])|(\W*(one|two|three|four|five|six|seven|eight|nine)\W*))";
            var lastDigitRegex = @".*(one|two|three|four|five|six|seven|eight|nine|[0-9]).*?$";

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D1-Input-test.txt"));
                line = sr.ReadLine();

                while (line != null)
                {
                    var firstDigit = Regex.Match(line, firstDigitRegex).Value;
                    var lastDigit = Regex.Matches(line, lastDigitRegex).ToList();
                    foreach(var digit in lastDigit){
                        Console.WriteLine(digit);
                    }
                    Console.WriteLine("first: " + firstDigit + " last: " + lastDigit[1]);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                result.Answer = answer.ToString();
                result.Message = "Trebuchet is launched";
            }

            return result;
        }
    }
}
