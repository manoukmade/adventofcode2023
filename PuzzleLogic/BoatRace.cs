﻿using AdventOfCode2023aspDOTnet.Models.Entities;
using System.Linq;

namespace AdventOfCode2023aspDOTnet.PuzzleLogic
{
    public class BoatRace
    {
        public static Solution SolveBoatRace(int part)
        {
            Solution result = new();
            string line;
            long answer = 1;

            List<long> time = new();
            List<long> distance = new();
            List<long> wonRaces = new();
            List<long> lineInts = new();

            try
            {
                using StreamReader sr = new(PuzzleBase.GetLocalInputFile("D6-Input.txt"));
                line = sr.ReadLine();
                while (line is not null)
                {
                    List<string> lineStrings = line.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
                    lineStrings.RemoveAt(0);
                    if (part == 1) 
                        lineInts = PuzzleBase.TranslateStringsToLong(lineStrings);
                    else 
                        lineInts = PuzzleBase.CompileTranslateStringsToLong(lineStrings);

                    _ = line.StartsWith("Time:") ? time = lineInts : distance = lineInts;
                    line = sr.ReadLine();
                }

                for (int r = 0; r < time.Count(); r++){
                    Console.WriteLine("checking race with time " + time[r] + " and distance " + distance[r]);
                    int won = 0;
                    for (int i = 0; i <= time[r]; i++) {
                        if (CheckWinningRace(i, time[r], distance[r]))
                            won++;
                    }
                    if (won > 0)
                        wonRaces.Add(won);
                }

                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                wonRaces.ForEach(w => answer *= w);
                result.Answer = answer.ToString();
                result.Message = "You won the grand prize";
            }
            return result;
        }

        public static bool CheckWinningRace(long x, long a, long y)
        {
            long calcY = (long)-Math.Pow(x,2) + (a * x);
            return calcY > y;
        }
    }
}
